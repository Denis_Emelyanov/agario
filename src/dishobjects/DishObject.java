/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package dishobjects;

/**
 *
 * @author DenisVlEm
 */
public abstract class DishObject {
    
    private int mass;
    
    public void setMass(int mass){
    }
    
    public int getMass(){
        return 0;
    }
    
    abstract protected void eat(DishObject other);
    
    abstract protected boolean canEat(DishObject other);
}
