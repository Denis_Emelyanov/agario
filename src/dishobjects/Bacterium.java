/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package dishobjects;

import dishobjects.DishObject;
import specs.PrimarySpecialization;
import specs.Specialization;

/**
 *
 * @author DenisVlEm
 */
public class Bacterium extends DishObject {
    
    public Bacterium(){
        currentSpec=new PrimarySpecialization(this);
    }
    private int speed;
    
    private Specialization currentSpec;
    
    private void setSpeed(int speed){
        
    }
    
    public void increaseMass(int d_mass){
        this.setMass(getMass()+d_mass);
    }
    
    private void decreaseMass(int d_mass){
        this.setMass(getMass()-d_mass);
    }
    public void setSpecialization(Specialization newSpec){
        currentSpec=newSpec;
    }
    public Specialization getSpec(){
        return currentSpec;
    }

    @Override
    protected void eat(DishObject other) {
        currentSpec.eat(other);
    }

    @Override
    protected boolean canEat(DishObject other) {
        return currentSpec.canEat(other);
    }
}
