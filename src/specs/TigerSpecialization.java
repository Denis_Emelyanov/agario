/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package specs;

import dishobjects.Bacterium;
import dishobjects.DishObject;
import dishobjects.ElementaryParticle;
import dishobjects.Oxygen;
import dishobjects.Water;

/**
 *
 * @author DenisVlEm
 */
public class TigerSpecialization extends Specialization{
    
    public TigerSpecialization(){
        super();
    }
    public TigerSpecialization(Bacterium owner){
        super(owner);
    }
    
    @Override
    public boolean canEat(DishObject other){
         boolean can = false;
         if(other instanceof  ElementaryParticle){
            if(other instanceof  Water){
                 can = true;
            }
            else if(other instanceof Oxygen){
                can = true;
            } 
         }
         else if(other instanceof Bacterium){
             can = canEat((Bacterium)other);
         }
         return can;
    }

    @Override
    protected boolean canEat(Bacterium bac) {
        
        boolean can = false;
        
        if(bac.getSpec() instanceof PrimaryAnimalSpecialization || bac.getSpec() instanceof HerbivoreAnimalSpecialization){
            if((bac.getMass()/this.getOwner().getMass())<=2)
                can = true;
        }
        else if (bac.getSpec() instanceof BuffaloSpecialization){
            if (((float)bac.getMass()/(float)this.getOwner().getMass()) <= 1.5f){
                can = true;
            }
        }
        return can;
    }

    @Override
    protected boolean canGrowUp() {
        boolean can = false;
        int countOfPrimary=0;
        int countOfHerbivore=0;
        int countOfBuffalo=0;
        int countOfWater=0;
        int countOfOxygen=0;
        for(DishObject obj : stomach){
            if(obj instanceof Bacterium){
                if(((Bacterium)obj).getSpec() instanceof PrimaryAnimalSpecialization){
                    countOfPrimary++;
                }
                else if(((Bacterium)obj).getSpec() instanceof HerbivoreAnimalSpecialization){
                    countOfHerbivore++;
                }
                else if(((Bacterium)obj).getSpec() instanceof BuffaloSpecialization){
                    countOfBuffalo++;
                }
            }
            else if(obj instanceof ElementaryParticle){
                if(obj instanceof Water){
                    countOfWater++;
                }
                else if(obj instanceof Oxygen){
                    countOfOxygen++;
                }
            }
        }
        if(countOfWater>=5&&countOfOxygen>=5&&countOfBuffalo>=2){
            can=true;
            //to do: to clean stomach
        }
        if(countOfWater>=5&&countOfOxygen>=5&&countOfPrimary>=7&&countOfHerbivore>=5){
            can=true;
            // to do to clean stomach
        }
        return can;
    }

    @Override
    public void eat(DishObject other) {
        stomach.add(other);
        //to do send message to model
        if(canGrowUp()){
            growUp(5);
        }    
    }
}
