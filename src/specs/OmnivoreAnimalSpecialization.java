/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package specs;

import dishobjects.*;

/**
 *
 * @author DenisVlEm
 */
public class OmnivoreAnimalSpecialization extends Specialization{
    
    public OmnivoreAnimalSpecialization(){
        super();
    }
    
    public OmnivoreAnimalSpecialization(Bacterium owner){
        super(owner);
    }

@Override
    public boolean canEat(DishObject other){
         boolean can = false;
         if(other instanceof  ElementaryParticle){
            if(other instanceof  Water){
                 can = true;
            }
            else if(other instanceof Oxygen){
                can = true;
            }
         }
         else if(other instanceof Bacterium){
             can = canEat((Bacterium)other);
         }
         return can;
    }

    @Override
    protected boolean canEat(Bacterium bac) {
        
        boolean can = false;
        
        if (bac.getSpec() instanceof PrimaryAnimalSpecialization){
            if (((float)bac.getMass()/(float)this.getOwner().getMass()) <= 2f){
                can = true;
            }
        }
        else if (bac.getSpec() instanceof HerbivoreAnimalSpecialization){
            if (((float)bac.getMass()/(float)this.getOwner().getMass()) <= 2f){
                can = true;
            }
        }       
        else if (bac.getSpec() instanceof PredatorAnimalSpecialization){
            if (((float)bac.getMass()/(float)this.getOwner().getMass()) <= 2f){
                can = true;
            }
        } 
        else if (bac.getSpec() instanceof OmnivoreAnimalSpecialization){
            if (((float)bac.getMass()/(float)this.getOwner().getMass()) <= 2f){
                can = true;
            }
        } 
        else if (bac.getSpec() instanceof TigerSpecialization){
            if (((float)bac.getMass()/(float)this.getOwner().getMass()) <= 2f){
                can = true;
            }
        } 
        else if (bac.getSpec() instanceof BuffaloSpecialization){
            if (((float)bac.getMass()/(float)this.getOwner().getMass()) <= 2f){
                can = true;
            }
        }
        else if(bac.getSpec() instanceof MossPlantSpecialization||
                bac.getSpec() instanceof ParasitePlantSpecialization||
                bac.getSpec() instanceof PredatorPlantSpecialization||
                bac.getSpec() instanceof PrimaryPlantSpecialization){
            if((bac.getMass()/this.getOwner().getMass())<=1.2f)
                can = true;
        }
        return can;
    }

    @Override
    protected boolean canGrowUp() {
        boolean can = false;
        int oxy=0;
        int water =0;
        int animal=0;
        int plant=0;
        for(DishObject obj : stomach){
            if(obj instanceof Bacterium){
                if(((Bacterium)obj).getSpec() instanceof PrimaryAnimalSpecialization||
                   ((Bacterium)obj).getSpec() instanceof HerbivoreAnimalSpecialization||
                   ((Bacterium)obj).getSpec() instanceof PredatorAnimalSpecialization||
                   ((Bacterium)obj).getSpec() instanceof OmnivoreAnimalSpecialization||
                   ((Bacterium)obj).getSpec() instanceof TigerSpecialization||
                   ((Bacterium)obj).getSpec() instanceof BuffaloSpecialization){
                    animal++;
                }
                else if(((Bacterium)obj).getSpec() instanceof MossPlantSpecialization||
                   ((Bacterium)obj).getSpec() instanceof ParasitePlantSpecialization||
                   ((Bacterium)obj).getSpec() instanceof PrimaryPlantSpecialization||
                   ((Bacterium)obj).getSpec() instanceof PredatorPlantSpecialization){
                    plant++;
                }
            }
            else if(obj instanceof ElementaryParticle){
                if(obj instanceof Water){
                    water++;
                }
                else if(obj instanceof Oxygen){
                    oxy++;
                }
            }
        }
        if(water>=5&&oxy>=5&&animal>=3){
            can=true;
        }
        if(water>=5&&oxy>=5&&plant>=3){
            can=true;
        }
            //to do: to clean stomach
        return can;
    }

    @Override
    public void eat(DishObject other) {
        stomach.add(other);
        //to do send message to model
        if(canGrowUp()){
            growUp(3);
        }    
    }
}
