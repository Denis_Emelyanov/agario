/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package specs;

import dishobjects.*;

/**
 *
 * @author DenisVlEm
 */
public class PredatorPlantSpecialization extends Specialization{
    
    public PredatorPlantSpecialization(){
        super();
    }
    
    public PredatorPlantSpecialization(Bacterium owner){
        super(owner);
    }

        @Override
    public boolean canEat(DishObject other){
         boolean can = false;
         if(other instanceof  ElementaryParticle){
            if(other instanceof  Water){
                 can = true;
            }
            else if(other instanceof DioxideCarbonate){
                can = true;
            }
            else if(other instanceof SunLight){
                can = true;
            } 
         }
         else if(other instanceof Bacterium){
             can = canEat((Bacterium)other);
         }
         return can;
    }

    @Override
    protected boolean canEat(Bacterium bac) {
        
        boolean can = false;
        

        if (bac.getSpec() instanceof PrimaryAnimalSpecialization){
            if (((float)bac.getMass()/(float)this.getOwner().getMass()) <= 0.5f){
                can = true;
            }
        }
        else if (bac.getSpec() instanceof HerbivoreAnimalSpecialization){
            if (((float)bac.getMass()/(float)this.getOwner().getMass()) <= 0.5f){
                can = true;
            }
        }
        else if (bac.getSpec() instanceof OmnivoreAnimalSpecialization){
            if (((float)bac.getMass()/(float)this.getOwner().getMass()) <= 0.5f){
                can = true;
            }
        }
        else if (bac.getSpec() instanceof PredatorAnimalSpecialization){
            if (((float)bac.getMass()/(float)this.getOwner().getMass()) <= 0.5f){
                can = true;
            }
        }
        
        return can;
    }

    @Override
    protected boolean canGrowUp() {
        boolean can = false;
        int co=0;
        int light=0;
        int water =0;
        int animal=0;
        for(DishObject obj : stomach){
            if(obj instanceof Bacterium){
                if(((Bacterium)obj).getSpec() instanceof PrimaryAnimalSpecialization||
                     ((Bacterium)obj).getSpec() instanceof HerbivoreAnimalSpecialization||
                        ((Bacterium)obj).getSpec() instanceof PredatorAnimalSpecialization||
                        ((Bacterium)obj).getSpec() instanceof OmnivoreAnimalSpecialization){
                    animal++;
                }  
            }
            else if(obj instanceof ElementaryParticle){
                if(obj instanceof Water){
                    water++;
                }
                else if(obj instanceof DioxideCarbonate){
                    co++;
                }
                else if(obj instanceof SunLight){
                    light++;
                }
            }
        }
        if(water>=3&&co>=3&&light>=3){
            can=true;
            //to do: to clean stomach
        }
        if(animal>=2){
            can=true;
            // to do to clean stomach
        }
        return can;
    }

    @Override
    public void eat(DishObject other) {
        stomach.add(other);
        //to do send message to model
        if(canGrowUp()){
            growUp(3);
        }    
    }
}
