/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package specs;

import dishobjects.*;

/**
 *
 * @author DenisVlEm
 */
public class PrimarySpecialization extends Specialization{
    
    public PrimarySpecialization(){
        super();
    }
    
    public PrimarySpecialization(Bacterium owner){
        super(owner);
    }

    @Override
    public boolean canEat(DishObject other){
         boolean can = false;
         if(other instanceof  ElementaryParticle){
            if(other instanceof  Water){
                 can = true;
            }
            else if(other instanceof SunLight){
                can = true;
            } 
            else if(other instanceof Agar){
                can = true;
            } 
         }
         else if(other instanceof Bacterium){
             can = canEat((Bacterium)other);
         }
         return can;
    }

    @Override
    protected boolean canEat(Bacterium bac) {
        
        boolean can = false;
        
        if(bac.getSpec() instanceof PrimarySpecialization){
            if((bac.getMass()<this.getOwner().getMass()))
                can = true;
        }
        return can;
    }

    @Override
    protected boolean canGrowUp() {
        boolean can = false;
        int primary=0;
        int agar=0;
        int water=0;
        int sunlight=0;
        for(DishObject obj : stomach){
            if(obj instanceof Bacterium){
                if(((Bacterium)obj).getSpec() instanceof PrimarySpecialization){
                    primary++;
                }
            }
            else if(obj instanceof ElementaryParticle){
                if(obj instanceof Water){
                    water++;
                }
                else if(obj instanceof Agar){
                    agar++;
                }
                else if(obj instanceof SunLight){
                    sunlight++;
                }
            }
        }
        if(water>=2&&agar>=2&&sunlight>=2&&primary>=2){
            can=true;
            //to do: to clean stomach
        }
        return can;
    }

    @Override
    public void eat(DishObject other) {
        stomach.add(other);
        //to do send message to model
        if(canGrowUp()){
            growUp(3);
        }    
    }
}
