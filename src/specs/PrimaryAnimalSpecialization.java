/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package specs;

import dishobjects.*;

/**
 *
 * @author DenisVlEm
 */
public class PrimaryAnimalSpecialization extends Specialization{
    public PrimaryAnimalSpecialization(){
        super();
    }
    
    public PrimaryAnimalSpecialization(Bacterium owner){
        super(owner);
    }

 @Override
    public boolean canEat(DishObject other){
         boolean can = false;
         if(other instanceof  ElementaryParticle){
            if(other instanceof  Water){
                 can = true;
            }
            else if(other instanceof Oxygen){
                can = true;
            }
         }
         else if(other instanceof Bacterium){
             can = canEat((Bacterium)other);
         }
         return can;
    }

    @Override
    protected boolean canEat(Bacterium bac) {
        
        boolean can = false;
        
        if(bac.getSpec() instanceof PrimarySpecialization){
            if((bac.getMass()/this.getOwner().getMass())<=1.2f)
                can = true;
        }
        return can;
    }

    @Override
    protected boolean canGrowUp() {
        boolean can = false;
        int primary=0;
        int water=0;
        int oxy=0;
        for(DishObject obj : stomach){
            if(obj instanceof Bacterium){
                if(((Bacterium)obj).getSpec() instanceof PrimarySpecialization){
                    primary++;
                }
            }
            else if(obj instanceof ElementaryParticle){
                if(obj instanceof Water){
                    water++;
                }
                else if(obj instanceof Oxygen){
                    oxy++;
                }
            }
        }
        if(water>=5&&oxy>=5&&primary>=3){
            can=true;
            //to do: to clean stomach
        }
        return can;
    }

    @Override
    public void eat(DishObject other) {
        stomach.add(other);
        //to do send message to model
        if(canGrowUp()){
            growUp(3);
        }    
    }
}
