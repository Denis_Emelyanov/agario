/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package specs;

import java.util.ArrayList;

/**
 *
 * @author DenisVlEm
 */
public class SpecializationTree {
    
    ArrayList <Specialization> getAvailableSpecialization(Specialization spec){
        ArrayList <Specialization> specs = new ArrayList <Specialization>();
        if(spec instanceof PrimarySpecialization){
            specs.add(new PrimaryPlantSpecialization());
            specs.add(new PrimaryAnimalSpecialization());
        }
        else if(spec instanceof PrimaryPlantSpecialization){
            specs.add(new MossPlantSpecialization());
            specs.add(new ParasitePlantSpecialization());
            specs.add(new PredatorPlantSpecialization());
        }
        else if(spec instanceof PrimaryAnimalSpecialization){
            specs.add(new OmnivoreAnimalSpecialization());
            specs.add(new PredatorAnimalSpecialization());
            specs.add(new HerbivoreAnimalSpecialization());
        }
        else if(spec instanceof HerbivoreAnimalSpecialization){
            specs.add(new BuffaloSpecialization());
        }
        else if(spec instanceof PredatorAnimalSpecialization){
            specs.add(new TigerSpecialization());
        }
       return specs; 
    }
}

