/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package specs;

import dishobjects.Bacterium;
import dishobjects.DishObject;
import java.util.ArrayList;


/**
 *
 * @author DenisVlEm
 */
public abstract class Specialization {
    
    protected  ArrayList <DishObject> stomach;
    protected Bacterium owner;
    
    public Specialization(){
        
    }
    
    public Specialization(Bacterium bac){
        this.setOwner(owner);
    }
    protected void setOwner(Bacterium _owner){
        owner = _owner;
    }
    
    protected Bacterium getOwner(){
        return owner;
    }
    
    abstract public void eat(DishObject other);
    abstract public boolean canEat(DishObject other);
    abstract protected boolean canEat(Bacterium bac);
    
    protected void changeSpecialization(Specialization spec){
        this.getOwner().setSpecialization(spec);
    }
    
    abstract protected boolean canGrowUp();
    
    protected void growUp(int d_mass){
        this.getOwner().increaseMass(d_mass);
    }
}
