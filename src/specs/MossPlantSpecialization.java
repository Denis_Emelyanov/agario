/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package specs;

import dishobjects.*;

/**
 *
 * @author DenisVlEm
 */
public class MossPlantSpecialization extends Specialization{
    
    public MossPlantSpecialization(){
        super();
    }
    
    public MossPlantSpecialization(Bacterium owner){
        super(owner);
    }
    @Override
    public boolean canEat(DishObject other){
         boolean can = false;
         if(other instanceof  ElementaryParticle){
            if(other instanceof  Water){
                 can = true;
            }
            else if(other instanceof SunLight){
                can = true;
            } 
            else if(other instanceof DioxideCarbonate){
                can = true;
            } 
         }
         
         return can;
    }

    @Override
    protected boolean canEat(Bacterium bac) {
        
        boolean can = false;
        
        return can;
    }

    @Override
    protected boolean canGrowUp() {
        boolean can = false;
        int co=0;
        int water=0;
        int sunlight=0;
        for(DishObject obj : stomach){
           if(obj instanceof ElementaryParticle){
                if(obj instanceof Water){
                    water++;
                }
                else if(obj instanceof DioxideCarbonate){
                    co++;
                }
                else if(obj instanceof SunLight){
                    sunlight++;
                }
            }
        }
        if(water>=2&&co>=2&&sunlight>=2){
            can=true;
            //to do: to clean stomach
        }
        return can;
    }

    @Override
    public void eat(DishObject other) {
        stomach.add(other);
        //to do send message to model
        if(canGrowUp()){
            growUp(3);
        }    
    }
}
