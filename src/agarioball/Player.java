/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package agarioball;

/**
 *
 * @author DenisVlEm
 */

import java.awt.*;
import java.awt.image.BufferedImage;

public class Player {

    private BufferedImage sprite;
    private String name;
    private Point position;

    Color color;

    public Player(String name) {
        this.name = name;
        position = new Point();
        color = new Color(
                (float) Math.random() % 192,
                (float) Math.random() % 192,
                (float) Math.random() % 192,
                1.0f);
        updateSprite();
    }

    public Color getColor(){
        return color;
    }

    public String getName() {
        return name;
    }

    public BufferedImage getSprite() {
        return sprite;
    }

    public double getX() {
        return position.getX();
    }

    public double getY() {
        return position.getY();
    }

    public void moveTo(double newX, double newY) {
        moveTo(newX, newY, 0.1);
    }

    public void moveTo(double newX, double newY, double speed) {
        position.move((int) (newX), (int) (newY));
    }
    
    private void updateSprite() {
        sprite = SpriteFactory.getSprite(50, name, color);
    }
}
