/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package agarioball;

import com.golden.gamedev.Game;
import com.golden.gamedev.GameLoader;
import com.golden.gamedev.object.Background;
import com.golden.gamedev.object.PlayField;
import com.golden.gamedev.object.Sprite;
import com.golden.gamedev.object.SpriteGroup;
import com.golden.gamedev.object.background.ImageBackground;
import com.golden.gamedev.object.collision.BasicCollisionGroup;
import com.golden.gamedev.object.collision.CollisionGroup;
import java.awt.Dimension;
import java.awt.Graphics2D;
import java.awt.image.BufferedImage;

/**
 *
 * @author DenisVlEm
 */
public class AgarioBall extends Game{

    static int fieldWidth = 640;
    static int fieldHeight = 480;
    private static int screenHeight = 1080, screenWidth = 1920;
    private Player humanPlayer;
    private PlayField field;
    private SpriteGroup sprGrp;
    private Background bg;
    private BasicCollisionGroup cm;
    double speed = 0.1;
    
    @Override
    public void initResources() {
        bg = new ImageBackground(getImage("res/BG.jpg"),1920,1080);
        field = new PlayField(bg);
        humanPlayer = new Player("Player");
        sprGrp = new SpriteGroup("Player");
        
        GameModel model=new GameModel();
        
        sprGrp.add(new Sprite(humanPlayer.getSprite(), 0, 0));
        sprGrp.setBackground(bg);
        field.addGroup(sprGrp);
        cm = new BallsCollision(sprGrp, sprGrp);
        cm.pixelPerfectCollision = true;
        setFPS(60);
    }

    @Override
    public void update(long elapsedTime) {
        bg.setToCenter(sprGrp.getActiveSprite());
        field.update(elapsedTime);
        sprGrp.getActiveSprite().update(elapsedTime);
        cm.checkCollision();
        cm.pixelPerfectCollision=true;
        Sprite s = sprGrp.getSprites()[0];
        int mx = bsInput.getMouseX();
        int my = bsInput.getMouseY();
        int bx = (int) (s.getX() + s.getWidth() / 2);
        int by = (int) (s.getY() + s.getHeight() / 2);
        int bgx = (int) bg.getX();
        int bgy = (int) bg.getY();
        int dx = mx + bgx - bx;
        int dy = my + bgy - by;
        if (Math.abs(dx) + Math.abs(dy) == 0) {
            s.setHorizontalSpeed(0);
            s.setVerticalSpeed(0);
        }
        else {  
            s.setHorizontalSpeed(speed * dx / (Math.abs(dx) + Math.abs(dy)));
            s.setVerticalSpeed(speed * dy / (Math.abs(dx) + Math.abs(dy)));
        }
        
        if (s.getX() <= 0 && s.getHorizontalSpeed() < 0)
            s.setHorizontalSpeed(0);
        if (s.getX() + s.getWidth() >= screenWidth && s.getHorizontalSpeed() > 0)
            s.setHorizontalSpeed(0);
        if (s.getY() <= 0 && s.getVerticalSpeed() < 0) {
            s.setVerticalSpeed(0);
        }
        if (s.getY() + s.getHeight() >= screenHeight && s.getVerticalSpeed() > 0)
            s.setVerticalSpeed(0);
    }

    @Override
    public void render(Graphics2D g) {
        field.render(g);
        sprGrp.render(g);
    }
    
    /**1
     * @param args the command line arguments
     */
    public static void main(String[] args) {
        GameLoader game = new GameLoader();
        game.setup(new AgarioBall(),
                new Dimension(fieldWidth, fieldHeight),
                false
        );
        game.start();
    }
    
}
