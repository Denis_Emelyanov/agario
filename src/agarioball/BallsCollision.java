/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package agarioball;

import com.golden.gamedev.object.Sprite;
import com.golden.gamedev.object.SpriteGroup;
import com.golden.gamedev.object.collision.BasicCollisionGroup;

/**
 *
 * @author DenisVlEm
 */
public class BallsCollision extends BasicCollisionGroup{

    public BallsCollision(SpriteGroup g1, SpriteGroup g2) {
        setCollisionGroup(g1, g2);
    }
    @Override
    public void collided(Sprite sprite, Sprite sprite1) {
        sprite.setActive(false);
        sprite1.setActive(false);
    }
    
}
