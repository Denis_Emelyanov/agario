/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package agarioball;

import java.awt.*;
import java.awt.image.BufferedImage;

/**
 * @author DenisVlEm
 */
public class SpriteFactory {

    public static BufferedImage getSprite(int radius, String text, Color color) {
        int diameter = radius * 2;
        BufferedImage circleBuffer = new BufferedImage(diameter, diameter, BufferedImage.TYPE_INT_ARGB);
        final Graphics2D circle2d = circleBuffer.createGraphics();

        // We drawing full circle first with darker color
        circle2d.setPaint(color);
        circle2d.fillOval(0, 0, diameter, diameter);

        // Now we making center a bit brighter
        circle2d.setPaint(color.brighter());
        circle2d.fillOval(
                (int) (radius * 0.1),
                (int) (radius * 0.1),
                (int) (radius * 1.8),
                (int) (radius * 1.8)
        );

        Font font = new Font("Comic Sans MS", Font.BOLD, 14);
        circle2d.setFont(font);
        FontMetrics fm = circle2d.getFontMetrics();

        circle2d.setPaint(color.darker().darker());
        circle2d.drawString(text,
                (diameter - fm.stringWidth(text)) / 2,
                (diameter) / 2 + 4
        );

        return circleBuffer;
    }
}
