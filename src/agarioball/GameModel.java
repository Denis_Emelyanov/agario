/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package agarioball;

import dishobjects.*;

/**
 *
 * @author DenisVlEm
 */
public class GameModel {
    
    public GameModel(){
        initGame();
    }
    
    public void addBacterium(Bacterium bac){
        PetryDish.getInstance().addBacterium(bac);
    }
    
    public void addElementaryParticle(ElementaryParticle particle){
        PetryDish.getInstance().addElementaryParticle(particle);
    }
    
    public void deleteObject(DishObject obj){
        PetryDish.getInstance().deleteObject(obj);
        
    }
    
    public void initGame(){
        
        for (int i=0; i<20; ++i){
            addBacterium(new Bacterium());
            addElementaryParticle(new Oxygen());
            addElementaryParticle(new DioxideCarbonate());
            addElementaryParticle(new Agar());
            addElementaryParticle(new Water());
        }
    }
}
