/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package agarioball;

import dishobjects.Bacterium;
import dishobjects.DishObject;
import dishobjects.ElementaryParticle;
import java.util.ArrayList;

/**
 *
 * @author DenisVlEm
 */


public class PetryDish {
    
    public static PetryDish getInstance(){
        return instance;
    }
    
    private static PetryDish instance = new PetryDish();
    
    private ArrayList <DishObject> bacteriums;
    
    private PetryDish(){
        bacteriums = new ArrayList <DishObject>();
    }
    
    public void addBacterium(Bacterium bac){
        bacteriums.add(bac);
    }
    public void addElementaryParticle(ElementaryParticle particle){
        bacteriums.add(particle);
    }
    public void deleteObject(DishObject obj){
        bacteriums.remove(obj);
    }
}
